<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */
?><!DOCTYPE html>
<html lang="tr-TR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true);
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" >
    <meta name="keywords" content="Ömer ipek, Ömer İPEK Kimdir?, Kişisel Web Sitesi, Kişisel Resmi sitesi, Ömeripek, Ömer, Portfolyo, PHP, Linux, JavaScript, Html 5, Css, Java, CMS, Mobil Uygulamalar, XML, JSON" />
    <title> <?php if(is_single()){ single_post_title('', true);}else { wp_title(); } ?></title>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/js/html5.js"></script>
	<script src="http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/js/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/style.css">
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.omeripek.com.tr/blog/feed/" />
    <link rel="alternate" type="text/xml" title="RSS .92" href="http://www.omeripek.com.tr/blog/feed/rss/" />
    <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="http://www.omeripek.com.tr/blog/feed/atom/" />
    <link rel="pingback" href="http://www.omeripek.com.tr/blog/xmlrpc.php" />
</head>

<body class="wrap">
<header role="banner">
    <div class="unit one-third show-on-mobiles">
        <h1>&nbsp;</h1>
    </div>
    <nav class="mobile-nav show-on-mobiles">
        <ul>
            <li>
                <a href="//www.omeripek.com.tr">/root</a>
            </li>
            <li>
                <a href="//www.omeripek.com.tr/blog">/blog</a>
            </li>
            <li>
                <a href="#"> /hakkımda</a>
            </li>
            <li>
                <a href="#">/cv </a>
            </li>

            <li>
                <a href="#">/iletişim</a>
            </li>
        </ul>


    </nav>
    <div class="grid">
        <div class="unit one-third center-on-mobiles">
            <a class="logo" href="//www.omeripek.com.tr" title="Ömer İpek"> <img src="http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/img/logo.png" alt="Ömer İPEK" /> </a>
        </div>
        <nav class="main-nav unit two-thirds hide-on-mobiles">
            <ul>
                <li>
                    <a href="//www.omeripek.com.tr">.. / anasayfa</a>
                </li>
                <li>
                    <a href="#"> / hakkımda</a>
                </li>
                <li>
                    <a href="#">/cv </a>
                </li>
                <li>
                    <a href="//www.omeripek.com.tr/blog">/ blog</a>
                </li>
                <li>
                    <a href="#">/ önemli</a>
                </li>
                <li>
                    <a href="#">/ irtibat</a>
                </li>
            </ul>
        </nav>

    </div>
</header>
<section class="docs">
    <div class="grid">
        <div class="docs-nav-mobile unit whole show-on-mobiles">
            <label>
                <select onchange="if (this.value) window.location.href=this.value">
                    <option value="">Son Yazılarım</option>
                    <optgroup label="">
                      <?php recentPost(); ?>
                    </optgroup>
                </select>
            </label>
        </div>
 <div class="unit four-fifths">