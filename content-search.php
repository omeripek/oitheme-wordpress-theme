<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php oiTheme_post_thumbnail(); ?>

	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
        <div class="improve">
            <?php oiTheme_entry_meta(); ?>
        </div>
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
    <div class="istatistik">
        <i class="fa fa-clock-o"></i> Makale Yayım Tarihi:  <?php the_time( get_option( 'date_format' ) ); ?>
    </div>
</article><!-- #post-## -->
<div class="isBot">

</div>