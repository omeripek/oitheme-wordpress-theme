<?php
/**
 * oiTheme back compat functionality
 *
 * Prevents oiTheme from running on WordPress versions prior to 4.1,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.1.
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */

/**
 * Prevent switching to oiTheme on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since oiTheme 1.0
 */
function oiTheme_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'oiTheme_upgrade_notice' );
}
add_action( 'after_switch_theme', 'oiTheme_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * oiTheme on WordPress versions prior to 4.1.
 *
 * @since oiTheme 1.0
 */
function oiTheme_upgrade_notice() {
	$message = sprintf( __( 'oiTheme requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'oiTheme' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Customizer from being loaded on WordPress versions prior to 4.1.
 *
 * @since oiTheme 1.0
 */
function oiTheme_customize() {
	wp_die( sprintf( __( 'oiTheme requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'oiTheme' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'oiTheme_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 4.1.
 *
 * @since oiTheme 1.0
 */
function oiTheme_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'oiTheme requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'oiTheme' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'oiTheme_preview' );
