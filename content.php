<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-header">
        <div class="improve right">
            <?php edit_post_link( __( '&nbsp; Düzenle', 'oiTheme' ), '<span class="edit-link"><i class="fa fa-pencil"></i>', '</span>' ); ?>

        </div>
        <?php
        if ( is_single() ) :
            the_title(sprintf( '<a href="%s" rel="bookmark"><h1 class="entry-title left">',esc_url( get_permalink() ) ), '</h1></a>' );
        else :
            the_title( sprintf( '<h2 class="entry-title left"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
        endif;
        ?>
    </div>

	<?php
		// Post thumbnail.
		oiTheme_post_thumbnail();
	?>
	<div class="entry-content">
        <div class="improve">
            <?php oiTheme_entry_meta(); ?>
        </div>


            <?php
                /* translators: %s: Name of current post */
                the_content( sprintf(
                    __( 'Devamını Oku... <i class="fa fa-angle-double-right"></i>', 'oiTheme' ),
                    the_title( '<span class="screen-reader-text">', '</span>', false )
                ) );

                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Sayfalar:', 'oiTheme' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '<span class="screen-reader-text">' . __( 'Sayfa', 'oiTheme' ) . ' </span>%',
                    'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
            ?>

	</div><!-- .entry-content -->


    <div class="istatistik">
        <i class="fa fa-clock-o"></i> Makale Yayım Tarihi:  <?php the_time( get_option( 'date_format' ) ); ?>

        <?php echo bi_kahve(); ?>
    </div>


	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>
    <div class="clear"></div>


</article><!-- #post-## -->
<div class="isBot">

</div>