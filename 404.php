<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area hataSayfa">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<header class="page-header">
                    <div><img style="width: 98%;" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/404.jpg" alt="404" /> </div>
					<h1 class="page-title"><?php _e( 'Oops! Aradığınız sayfaya şu anda ulaşılamıyor. Belki de böyle bir sayfa aramıyordunuz. Belki de aradığınız bu sayfadır. Veya site çöktü çaktırmayın! ', 'oiTheme' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content" style="margin: 10px auto; width: 300px;">
					<p><?php _e( 'Aramaya yapmayı deneyebilirsiniz. <br />(Arama kısmıda çalışmıyorsa bittim ben :( )', 'oiTheme' ); ?></p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
