<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */
?>

</div>

<div class="unit one-fifth hide-on-mobiles">
    <aside>
        <?php get_sidebar(); ?>
    </aside>
</div>

<div class="clear"></div>

	</div><!-- .site-content -->
</section>

<footer class="footer" role="contentinfo">
    <div class="bgFoter">

    </div>
    <div class="kapsa">
        <div class="grid">
            <div class="unit one-third center-on-mobiles">
                <p>Ömer İpek - Kişisel Web Sitesi</p>
            </div>
            <div class="unit two-thirds align-right
             center-on-mobiles">
               <div class="sosyal">
                   <a href="#"> <i class="fa fa-linux"></i> </a>
                   <a href="#"> <i class="fa fa-apple"></i> </a>
                   <a href="#"> <i class="fa fa-firefox"></i> </a>
                   <a href="#"> <i class="fa fa-github"></i> </a>
                   <a href="#"> <i class="fa fa-stack-overflow"></i> </a>
                   <a href="#"> <i class="fa fa-twitter"></i> </a>
                   <a href="#"> <i class="fa fa-dribbble"></i> </a>
                   <a href="#"> <i class="fa fa-linkedin"></i> </a>
               </div>
            </div>

        </div>
    </div>
</footer>


<?php wp_footer(); ?>
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter25477769 = new Ya.Metrika({id:25477769,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<script>
    var anchorForId = function (id) {
        var anchor = document.createElement("a");
        anchor.className = "header-link";
        anchor.href = "#" + id;
        anchor.innerHTML = "<span class=\"sr-only\">Permalink</span><i class=\"fa fa-link\"></i>";
        anchor.title = "Permalink";
        return anchor;
    };

    var linkifyAnchors = function (level, containingElement) {
        var headers = containingElement.getElementsByTagName("h" + level);
        for (var h = 0; h < headers.length; h++) {
            var header = headers[h];

            if (typeof header.id !== "undefined" && header.id !== "") {
                header.appendChild(anchorForId(header.id));
            }
        }
    };

    document.onreadystatechange = function () {
        if (this.readyState === "complete") {
            var contentBlock = document.getElementsByClassName("docs")[0] || document.getElementsByClassName("news")[0];
            if (!contentBlock) {
                return;
            }
            for (var level = 1; level <= 6; level++) {
                linkifyAnchors(level, contentBlock);
            }
        }
    };
</script>

<script src='http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/js/jquery.js'></script>
<script src="http://www.omeripek.com.tr/blog/wp-content/themes/oiTheme/js/index.js"></script>
</body>
</html>
