$('.comments-area').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active cbhl');
        } else {
          label.addClass('active cbhl');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active cbhl'); 
			} else {
		    label.removeClass('cbhl');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('cbhl'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('active cbhl');
			}
    }

});