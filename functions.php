<?php
/**
 * oiTheme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage oiTheme
 * @since oiTheme 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since oiTheme 1.0
 */
    if ( ! isset( $content_width ) ) {
        $content_width = 660;
    }

/**
 * oiTheme only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'oiTheme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since oiTheme 1.0
 */
function oiTheme_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on oiTheme, use a find and replace
	 * to change 'oiTheme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'oiTheme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'oiTheme' ),
		'social'  => __( 'Social Links Menu', 'oiTheme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = oiTheme_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'oiTheme_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css' ) );
}
endif; // oiTheme_setup
add_action( 'after_setup_theme', 'oiTheme_setup' );

/**
 * Register widget area.
 *
 * @since oiTheme 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function oiTheme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'oiTheme' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'oiTheme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'oiTheme_widgets_init' );


/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since oiTheme 1.1
 */
function oiTheme_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'oiTheme_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since oiTheme 1.0
 */
function oiTheme_scripts() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'oiTheme-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

}
add_action( 'wp_enqueue_scripts', 'oiTheme_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since oiTheme 1.0
 *
 * @see wp_add_inline_style()
 */
function oiTheme_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'oiTheme-style', $css );
}
add_action( 'wp_enqueue_scripts', 'oiTheme_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since oiTheme 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function oiTheme_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'oiTheme_nav_description', 10, 4 );


/**
 * Implement the Custom Header feature.
 *
 * @since oiTheme 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since oiTheme 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since oiTheme 1.0
 */
require get_template_directory() . '/inc/customizer.php';

function recentPost(){
    $args = array( 'numberposts' => '5' );
    $recent_posts = wp_get_recent_posts( $args );
        foreach( $recent_posts as $recent ){
            echo '<option value=' . get_permalink($recent["ID"]) . '>' .   $recent["post_title"].'</option>';
        }
}


/**
 *  Soyal Medyada paylaşma eklentisi.
 */

function oi_sosyal_buton($content) {
    global $post;
    $permalink = get_permalink($post->ID);
    $title = get_the_title();
    if(!is_feed() && !is_home() && !is_page()) {
        $content = $content . '
         <div class="icerikFooter">
            <div class="oiSosyal">
                <a class="icon-twitter" href="http://twitter.com/share?text='.$title.'&url='.$permalink.'"  title="Twitter üzerinde paylaş" rel="nofollow"
                    onclick="window.open(this.href, \'twitter-share\', \'width=550,height=320\');return false;">
                    <i class="fa fa-twitter-square fa-2x"></i>
                </a>

                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;title='.$title.'&amp;url='.$permalink.'" title="Linkedin üzerinde paylaş" rel="nofollow"
                    onclick="window.open(this.href, \'Linkedin-share\', \'width=550,height=450\');return false;">
                    <i class="fa fa-linkedin-square fa-2x"></i>
                </a>

                <a class="icon-fb" href="https://www.facebook.com/sharer/sharer.php?u='.$permalink.'" title="Facebook Üzerinde paylaş!" rel="nofollow"
                     onclick="window.open(this.href, \'facebook-share\',\'width=580,height=400\');return false;">
                    <i class="fa fa-facebook-square fa-2x"></i>
                </a>

                <a class="icon-gplus" href="https://plus.google.com/share?url='.$permalink.'" title="Google Plus üzerinde paylaş!" rel="nofollow"
                   onclick="window.open(this.href, \'google-plus-share\', \'width=490,height=530\');return false;">
                   <i class="fa fa-google-plus-square fa-2x"></i>
                </a>

                <a href="http://www.reddit.com/submit?url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>" title="Reddit üzerinde oyla" rel="nofollow"
                 onclick="window.open(this.href, \'reddit-share\', \'width=490,height=530\');return false;">
                   <i class="fa fa-reddit-square fa-2x"></i>
                </a>
        </div>
        </div>

    ';
    }
    return $content;
}

add_filter('the_content', 'oi_sosyal_buton');

function bi_kahve() {
    if(!is_feed() && !is_home() && !is_page()) {
       return '<a href="https://www.paypal.me/omripk/3" target="_blank" rel="nofollow" title=" Bi kahve ısmarlar mısın? :)"><div class="biKahve"></div> </a>';
    }
}