<form method="get" id="search-2" class="search-form" action="<?php bloginfo('url'); ?>">
    <div class="aramaAlani">
        <label for="s"></label>
        <input type="search" value="<?php if(trim(wp_specialchars($s,1))!='') echo trim(wp_specialchars($s,1));else echo ' ';?>" name="s" id="s" placeholder="Arama" />
        <button type="submit" class="search-submit">
            <i class="fa fa-search"></i>
        </button>
    </div>
</form>